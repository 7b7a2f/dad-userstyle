# Dad.gallery custom CSS style

## What it this

![Main page](preview1.png)
![Submissions page](preview2.png)

## How to install

1. Install Stylus extension:
   - [For Chrome](https://chrome.google.com/webstore/detail/stylus/clngdbkpkpeebahjckkjfobafhncgmne)
   - [For Firefox](https://addons.mozilla.org/en-US/firefox/addon/styl-us/)
2. Open this [page](https://gitlab.com/7b7a2f/dad-userstyle/-/raw/master/dad.user.css).
3. Press "Install style" button.
